﻿using System;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace HomeWork_6v2
{
    class Program
    {
        #region Методы выделения последовательностей

        /// <summary>
        /// Вычисляет последовательность простых чисел методом "Решето Эрастофена"
        /// </summary>
        /// <param name="list">Исходный массив битов. ненужные элементы заменябтся на true</param>
        /// <returns>Возвращает массив, где false = простые числа</returns>
        static BitArray ErastofenBool(BitArray list)
        {
            BitArray arr = new BitArray(list);
            for (int i = 2; i  < arr.Length; i++)
            {
                if (!list[i])
                {
                    for (int j = i * 2; j < arr.Length; j += i)
                    {
                        arr[j] = true;
                    }
                }
            }
            return arr;
        }
        #endregion
        #region Работа с файлами

        /// <summary>
        /// Считывание числа из файла
        /// </summary>
        /// <param name="path">Полный путь к файлу</param>
        /// <returns>Возвращает число</returns>
        static int ReadFile(string path)
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                return int.Parse(sr.ReadLine());
            }
        }
        /// <summary>
        /// Проверка существования файла ввода
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        static void IsInputFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine($"Файл данных inputNumber.txt не найден. Создаю файл по умолчанию");
                using (StreamWriter sw = new StreamWriter(new FileStream("inputNumber.txt", FileMode.Create, FileAccess.Write)))
                {
                    sw.WriteLine("1000");
                }
            }
        }

        /// <summary>
        /// Архивация файла стандартными средствами
        /// </summary>
        /// <param name="source">Исходный файл</param>
        /// <param name="output">Выходной файл архива</param>
        static void Compressig(string source, string output)
        {
            if (!File.Exists(source)) Console.WriteLine("Файл не существует! Создайте файл для архивации или проверьте правильность пути");
            else
            {
                using (FileStream fs = new FileStream(source, FileMode.Open))
                {
                    using (FileStream nf = File.Create(output))
                    {
                        using (GZipStream gs = new GZipStream(nf, CompressionMode.Compress))
                        {
                            fs.CopyTo(nf);
                        }
                    }
                }
            }
        }
        #region Вспомогательные методы обработки массивов
        /// <summary>
        /// Вычисляем количество групп чисел, не делящихся друг на друга (степень двойк плюс единица)
        /// </summary>
        /// <param name="number">Количество чисел в последовательности от нуля</param>
        /// <returns>Возвращает количество групп чисел, не делящихся друг на друга</returns>
        static int ReturnGroupCount(int number)
        {
            int count = 1;
            while (number > 1)
            {
                number /= 2;
                count++;
            }
            return count;
        }
        /// <summary>
        /// Метод выводящий строку с замером времени выполнения алгоритма в секундах.
        /// </summary>
        /// <param name="sv">Stopwatch</param>
        static void PrintTime(Stopwatch sv)
        {
            Console.WriteLine("Время выполнения алгоритма вычисления: " + sv.Elapsed.TotalSeconds + " сек.");
        }
        #endregion

        #endregion
        static void Main(string[] args)
        {
            string inputFile = @"inputNumber.txt";
            IsInputFile(inputFile);
            Console.WriteLine("Загружаю число из файла...");
            int number = ReadFile(inputFile);                           //Считываю txt файл
            int[] res = new int[ReturnGroupCount(number)];              //Массив массивов для хранения последовательностей чисел
            res[0] = 1;                                                 //Первая последовательность = 1
            Stopwatch sv = new Stopwatch();                             //Таймер для замера времени выполнения задачи
            int[][] result = new int[ReturnGroupCount(number)][];       //Массив массивов для хранения последовательностей чисел
            result[0] = new int[] { 1 };
            var num = new List<uint>();
            for(var i = 1u;i<number;i++)
            {
                num.Add(i);
            }
            Console.WriteLine("Внимание! Число в файле = " + number);
            Console.WriteLine();
            Console.WriteLine("Выберите режим работы: \n" +
                "1 - Показать только кол-во групп\n" +
                "2 - Выполнить алгоритм подсчета групп чисел методом перебора значений\n" +
                "с последующей архивацией или сохранением в файл\n" +
                "Требуется мощный процессор. Миллиард не обработает\n" +
                "3 - Быстрый алгоритм, не требует большого объема памяти.\n" +
                "Возможно обработает Миллиард значений. Но нужен мощный процессор");
            switch (Console.ReadLine())
            {
                #region case "1": //Показать только кол-во групп
                case "1":
                    int[] range = new int[number - 2];
                    sv.Start();
                    Console.WriteLine("Групп чисел: " + ReturnGroupCount(number));
                    sv.Stop();
                    PrintTime(sv);
                    break;
                #endregion
                #region "3": //Решето Эратосфена
                case "2":
                    Console.WriteLine("Формирую массив чисел от 1 до вашего числа");
                    sv.Start();
                    BitArray number_s = new BitArray(number);
                    number_s[0] = true;
                    sv.Stop();
                    PrintTime(sv);
                    Console.WriteLine();
                    Console.WriteLine("Начинаю рассчет последовательностей чисел...");
                    sv = new Stopwatch();
                    sv.Start();
                    using (StreamWriter sw = new StreamWriter("output2.txt"))
                    {
                        sw.Write(1);
                        for (int i = 1; i < result.Length; i++)
                        {
                            Stopwatch s = new Stopwatch();
                            s.Start();
                            BitArray buffer = ErastofenBool(number_s);
                            for (int m = 2; m < buffer.Length; m++)
                            {
                                if (!buffer[m])
                                {
                                    if  ((m & (m - 1)) == 0)
                                    {
                                        sw.WriteLine();
                                        sw.Write(m);
                                        sw.Write(" ");
                                        if (number_s[m] == buffer[m] && !number_s[m])number_s[m] = !number_s[m];
                                    }
                                    else
                                    {
                                        sw.Write(m);
                                        sw.Write(" ");
                                        if (number_s[m] == buffer[m] && !number_s[m]) number_s[m] = !number_s[m];
                                    }
                                }
                            }
                            s.Stop();
                            Console.WriteLine($"Последовательность № {i} сформирована за {s.Elapsed.TotalSeconds} сек.");
                        }
                        sw.Flush();
                    }
                    sv.Stop();
                    PrintTime(sv);

                    Console.WriteLine("Производим запись результатов в файл output2.txt");
                    Console.WriteLine("Запись окончена!");
                    Console.WriteLine("Заархивировать полученный файл? \n" +
                    "1 - Да\n" +
                    "0 - Нет, просто выйти из программы");
                    string files = "output2.txt";
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Compressig(files, "archivedOutput2.zip");
                            Console.WriteLine("Файл заархивирован. Ищите его в папке с exe файлом");
                            break;
                        case "0":
                            break;
                    }
                    break;
                    #endregion
            }
        }
    }
}
